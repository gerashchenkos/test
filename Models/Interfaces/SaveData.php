<?php

namespace Shop\Models\Interfaces;

interface SaveData
{
    public function save();
}